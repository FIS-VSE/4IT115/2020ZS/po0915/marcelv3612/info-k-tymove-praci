# Informace k týmovému projektu

Týmový projekt by se podle konvencí měl jmenovat názvem aplikace.

Řídí se primárně zadáním na https://java.vse.cz/4it115/ZadaniDruheUlohy

Předtím, než budete zakládat README je třeba založit a nahrát prázdný Maven projekt, tj. soubor pom.xml (viz skripta anebo vykopírujte z adventury). Pokud byste vytvořili README dříve, nemohli byste lokální repozitář napojit na vzdálený.

Pokud ještě nechcete vytvářet prázdný projekt, můžete začít wiki, která ale bude obsahovat i informace, které by měly být později v README. Wiki sdílení z lokálního na vzdálený repozitář neovlivňuje - je to separátní repozitář.

# Co dát do README?

Základní informace o projektu
* skutečný název aplikace, např. Továrna na čokoládu
* anotace v jedné větě, např. Willy Wonka a oompa-loompové nabízejí na zakázku ručně vyrobenou čokoládu dle vlastních požadavků
* seznam členů týmu
* širší anotace, která vysvětlí problém, který aplikace řeší
* seznam funcionality ve stylu user stories
  * Jako <*aktér*> chci <*použít funkcionalitu*>, abych <*něčeho dosáhl*>.

# Co dát do wiki?

Wiki vytvoříte ve vašem týmovém projektu. Povinná je výchozí stránka home, ale obsah můžete i dále strukturovat.

Wiki bude obsahovat to, čemu se v [zadání](https://java.vse.cz/4it115/ZadaniDruheUlohy) říká "Návrh řešení".

* to samé, co je v README
* UML modely
    * use case diagram - obrázek z CASE nástroje    
      * pro každý use case slovní popis včetně scénářů dle šablony z metodiky MMSP
    * class diagram na designové úrovni vytvořený v CASE nástroji - oddělení tříd s uživatelským rozhraním a tříd logiky do balíčků
* strutkura úložiště dat aplikace (nikoli souborů v projektu)
* návrh uživatelského rozhraní 
  * UI flow diagram, 
  * náčrtky nebo prototypy obrazovek
* návrh testovacích případů